.PHONY: all dotfiles config

all: dotfiles config

dotfiles:
	git clone https://gitlab.com/i0z0m/dotfiles.git ~/dotfiles
	cd ~/dotfiles
	make deploy
	./set.sh

config:
	git clone https://gitlab.com/i0z0m/config.git ~/config
	cd ~/config
	make deploy
	./set.sh
