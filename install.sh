#!/bin/sh
set -e

# common
sudo apt-get update
sudo apt-get upgrade

# zsh
sudo apt-get install zsh
sudo apt-get install mosh
sudo apt-get install tmux
sudo apt-get install fzf
sudo apt-get install bat
sudo apt-get install ripgrep
sudo apt-get install keychain

echo "Finished!"
